const gulp = require(`gulp`);
const sass = require(`gulp-sass`);
const plumber = require(`gulp-plumber`);
const postcss = require(`gulp-postcss`);
const autoprefixer = require(`autoprefixer`);
const sourcemaps = require(`gulp-sourcemaps`);
const watch = require(`gulp-watch`);
const mqpacker = require(`css-mqpacker`);
const cssnano = require('gulp-cssnano');
const rename = require(`gulp-rename`);

gulp.task(`style`, () => {
  return gulp.src(`src/style.scss`).
  pipe(plumber()).
  pipe(sourcemaps.init()).
  pipe(sass()).
  pipe(postcss([
    autoprefixer({
      browsers: ['last 3 versions'],
    }),
    mqpacker({sort: true})
  ])).
  pipe(cssnano()).
  pipe(sourcemaps.write(`.`)).
  pipe(gulp.dest(`./`))
});



gulp.task(`watch`, () => {
  gulp.watch(`src/**/*.{scss,sass}`, [`style`]);
});

gulp.task(`build`, () => {
  gulp.start(`style`);
});